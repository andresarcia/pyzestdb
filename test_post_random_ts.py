"""
.. module:: test_post_random_ts
    :platform: Linux, Mac OS
    :synopsis: Sample code for posting a random value into a /foo/bar.

.. moduleauthor:: Andres Arcia-Moret<andres.arcia@cl.cam.ac.uk>

.. rubric:: test_get_last_ts.py

**POST a time series value**
    An example of the `POST value /foo/bar` call for time series DB.

"""
import dbox_zest_message
from dbox_zest_message import Zest_Response_Message
from dbox_zest_commbase import Zest_Access_Point
import random

if __name__ == '__main__':
    end_point = 'tcp://128.232.60.105:5555'
    token = ''
    sensor_name = 'sensor'
    rnumber = int(random.uniform(1,100))
    payload = "{\"value\": "+str(rnumber)+"}"

    myAP = Zest_Access_Point(end_point, token)

    raw_msg = myAP.post_ts(sensor_name, payload)

    print("last value in >sensor<\n", raw_msg)

    m = Zest_Response_Message(raw_msg)
    #
    # (format, msg)  = m.get_payload()
    #
    # if format == dbox_zest_message.ZEST_JSON_FORMAT:
    print(m.get_response_code())