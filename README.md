# pyzestdb

For complete information about Zest please visit:

https://github.com/jptmoore/arbiter

and 

https://me-box.github.io/zestdb/
https://github.com/me-box/zestdb

and 

https://github.com/jptmoore/nibbledb

1. Start a Zest server

For PC:
docker run -p 5555:5555 -p 5556:5556 -d --name zest --rm jptmoore/zestdb /app/zest/server.exe --enable-logging --secret-key-file example-server-key

For ARM (raspberry pi):

Server:
docker run -p 5555:5555 -p 5556:5556 -d --name zest --rm jptmoore/zestdb:aarch64 /home/databox/server.exe --secret-key-file example-server-key

(you can replace deamon mode -d by interactive mode -it to debug, also remember to add --enable-logging for the later)

in debug mode, then 
docker logs -f zest
docker logs -t zest 

Client:

Posting {"value": 42}:

docker run --network host -it jptmoore/zestdb:aarch64 /home/databox/client.exe --server-key 'vl6wu0A@XP?}Or/&BR#LSxn>A+}L)p44/W[wXL3<' --path '/ts/sensor' --mode post --payload '{"value": 42}' --request-endpoint tcp://128.232.60.105:5555

Observation 

1. run a client in observe mode
$ docker run --network host -it jptmoore/zestdb /app/zest/client.exe --server-key 'vl6wu0A@XP?}Or/&BR#LSxn>A+}L)p44/W[wXL3<' --path '/kv/foo/bar' --mode observe --request-endpoint tcp://128.232.60.105:5555

2. send data to the store (posting Key/Value, remember the path is /kv/id/key) 
docker run --network host -it jptmoore/zestdb /app/zest/client.exe --server-key 'vl6wu0A@XP?}Or/&BR#LSxn>A+}L)p44/W[wXL3<' --path '/kv/foo/bar' --payload '{"name":"dave", "age":30}' --mode post --request-endpoint tcp://128.232.60.105:5555
