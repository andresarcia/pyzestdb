"""
.. module:: test_debug_message
    :platform: Linux, Mac OS
    :synopsis: Testing the debug message manager.

.. moduleauthor:: Andres Arcia-Moret<andres.arcia@cl.cam.ac.uk>

.. rubric:: test_debug_message.py

Simple test of message debugger. Messages are similar to those given by the Reason code.
"""

import dbox_zest_message
from dbox_zest_message import Zest_Observe_Data_Message, Zest_Response_Message, Zest_Message_Debugger, Zest_GET_Message_Timeseries, ZEST_JSON_FORMAT
from dbox_zest_commbase import Zest_Access_Point


if __name__ == '__main__':
    md = Zest_Message_Debugger()

    # control_end_point = 'tcp://128.232.60.105:5555'
    token = ''
    sensor_name = 'sensor'
    uri = "/ts/" + sensor_name + "/last/1"

    # print('Time series message')
    #
    # m = Zest_GET_Message_Timeseries(token, uri, ZEST_JSON_FORMAT)
    #
    # md.print_in_hex(m.get_message())

    uri = "/kv/foo/bar"
    m = Zest_Observe_Data_Message(token, uri, ZEST_JSON_FORMAT, 0)

    print('debug note: remember that printed host name may differ because reference is running from a container')

    md.print_in_hex(m.get_message())

    #md.print_in_hex("hola como estan")

    #
    # myAP = Zest_Access_Point(end_point, token)
    #
    # raw_msg = myAP.get_last_ts(sensor_name)
    # #print("last value in >sensor<\n", raw_msg)
    #
    # m = Zest_Response_Message(raw_msg)
    #
    # (format, msg)  = m.get_payload()
    #
    # if format == dbox_zest_message.ZEST_JSON_FORMAT:
    #     print(msg)