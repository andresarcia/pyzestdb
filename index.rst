.. pyzestdb documentation master file, created by
   sphinx-quickstart on Mon Sep 24 18:06:44 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyzestdb's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Table of Content:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


PyZestDB client 
===============

PyZestDB is a python interface based on the Reason client for ZestDB. 
please find (Reason) ZestDB code here: https://me-box.github.io/zestdb/ 
and ZestDB paper here: https://arxiv.org/abs/1902.07009


.. automodule:: dbox_zest_commbase

.. autoclass:: dbox_zest_commbase.Zest_Access_Point
   :members: get_last_ts, get_last_n_ts, post_ts, observe

.. automodule:: dbox_zest_message

.. automodule:: test_debug_message 

.. automodule:: test_get_last_ts

.. automodule:: test_post_random_ts 

.. automodule:: test_observer 
