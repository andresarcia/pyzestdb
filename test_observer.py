"""
.. module:: test_observer
    :platform: Linux, Mac OS
    :synopsis: Sample code for an observer running independently.

.. moduleauthor:: Andres Arcia-Moret<andres.arcia@cl.cam.ac.uk>

.. rubric:: test_observer.py

**The Observer**
    An example of an observer. The current process subscribes to the end-point (ie the uri-to-observe at ZestDB)
    and prints the observations to the console.

"""

import dbox_zest_message
from dbox_zest_message import Zest_Observe_Data_Message, Zest_Response_Message, Zest_Message_Debugger, Zest_GET_Message_Timeseries, ZEST_JSON_FORMAT
from dbox_zest_commbase import Zest_Access_Point

if __name__ == '__main__':
    md = Zest_Message_Debugger()

    rest_end_point = 'tcp://127.0.0.1:5555' # REST
    pubsub_end_point = 'tcp://127.0.0.1:5556'    # PUB/SUB

    uri_to_observe = '/kv/foo/bar'
    token = ''

    # observing in 'data' mode by default
    myAP = Zest_Access_Point(rest_end_point, pubsub_end_point, token)
    my_obs_id = myAP.connect_observer(uri_to_observe)

    #wait for messages to observe
    while (True):
        print(str(myAP.observe(my_obs_id)))

