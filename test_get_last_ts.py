"""
.. module:: test_get_last_ts
    :platform: Linux, Mac OS
    :synopsis: Sample code for an observer running independently.

.. moduleauthor:: Andres Arcia-Moret<andres.arcia@cl.cam.ac.uk>

.. rubric:: test_get_last_ts.py

**GET last time series value**
    An example of the `GET last` call. Also notice the processing of the message to get it in the right format.

"""
import dbox_zest_message
from dbox_zest_message import Zest_Response_Message
from dbox_zest_commbase import Zest_Access_Point

if __name__ == '__main__':
    end_point = 'tcp://128.232.60.105:5555'
    token = ''
    sensor_name = '/my/sensor' # also URI

    myAP = Zest_Access_Point(end_point, token)

    raw_msg = myAP.get_last_ts(sensor_name)

    print("last value in >/my/sensor< in raw format\n", raw_msg)

    m = Zest_Response_Message(raw_msg)

    (format, msg)  = m.get_payload()

    if format == dbox_zest_message.ZEST_JSON_FORMAT:
        print(msg)
    else:
        print("Error: I'm not able to process this format")