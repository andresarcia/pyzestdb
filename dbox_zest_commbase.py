"""
.. module:: dbox_zest_commbase
   :platform: Linux, Mac OS
   :synopsis: Provides an access point to ZestDB and all communication for timeseries and key-value primitives to the database.

.. moduleauthor:: Andres Arcia-Moret<andres.arcia@cl.cam.ac.uk>

.. rubric:: dbox_zest_commbase.py

This module handles the establishment of a connection between a client and a ZestDB. It also provides primitives to
access data within the Store component.
This module provides the calls GET, POST, DELETE  and OBSERVE thus implementing the zest protocol from the client side.
A Zest_Access_Point provides (1) a connection to ZestDB and (2) the methods to access the time series and key-value data.

Zest_Access_Point uses two channels to interact with ZestDB: the synchronous port (or control channel at 5555)
implementing the REST/RPC-like protocol, and the asynchronous channel (or data channel at 5556) implementing
the PUB/SUB semantic including various purposes such as Observing general or specific dynamics of the
DB or so-called Store in the server. The data channel is encrypted and needs a special handshaking with ZestDB
(via the ZMQ dealer socket) initially triggered from the control channel.

Observers rely on the router/dealer communication pattern of ZMQ. They leverage on the enhanced
fully distributed communication provided by ZMQ library, so at transmission time, copies of messages
within the Databox are handled by ZMQ. For example, the ZMQ Router living in the Databox is able to
copy a client POST to the Store and the subscribers of the given path (TS, KV, etc) efficiently
disseminating information to all subscribed components.

    You have to provide an **end_point** and a **token**.

    .. note::

       Also the following directories and files must be in place:

       private_keys/client.key_secret\n
       private_keys/server.key_secret\n
       public_keys/client.key\n
       public_keys/server.key

As you can see later in the examples, the :mod:`Zest_Access_Point` class interacts with the :mod:`Zest_Message_Manager`
class as shown in the following picture. The later class is used to create and process the different messages for Zest protocol.

.. figure:: architecture-zest.jpg
    :width: 450px
    :align: center
    :alt: Zest Architecture
    :figclass: align-center



"""

import os
import zerorpc
import zmq
import zmq.auth
import struct
import socket
import time
import random

#
# from zerorpc.cli import parser
#

from dbox_zest_message import *

# end_point is of type: "tcp://128.232.60.105:5555"


class Zest_Access_Point:
    """
    Access Point to a ZestDB server, it connects to a ZMQ end point of the type "tcp://192.168.0.14:5555".

    Note:
        Do not include the `self` parameter in the ``Args`` section.
	`Zest_Access_Point` is under development so some of method's signatures can change.

    ``Args``:
        control_end_point(str): Databox end point of ZMQ of the type tcp://192.168.0.1:5555; this end point is intended to send request
        and receive replies from the control socket to convey RPC-like interactions .\n
        data_end_point(str): Databox end point of ZMQ of the type tcp://192.168.0.1:5556; this end point is a subscription for data (for
        monitoring purposes), audit (meta-data) or notifications (secure RPCs).\n

	token(str): Token provided by Databox arbiter.\n
        code (:obj:`int`, optional): Error code.\n

    ``Attributes``:
        base_dir: preferably current directory where your client is.\n
        keys_dir: certificates\n
        public_keys_dir: directory for public keys\n
        secret_keys_dir: directory for secret keys\n
	code (int): Exception error code.

    """

    def __init__(self, control_end_point, data_end_point, token):
        # SECURE = True
        self.control_end_point = control_end_point
        self.data_end_point = data_end_point
        self.base_dir = os.path.dirname(__file__)
        self.keys_dir = os.path.join(self.base_dir, 'certificates')
        self.public_keys_dir = os.path.join(self.base_dir, 'public_keys')
        self.secret_keys_dir = os.path.join(self.base_dir, 'private_keys')
        self.token = token

        self.valid_observers = ()
        self.observer_id = 0

        self.socket = self.config_connection()

        context = zmq.Context()
        self.data_ch = context.socket(zmq.DEALER)


    def setup_links(self, args, socket):
        if args.bind:
            for endpoint in args.bind:
                print('binding to "{0}"'.format(endpoint))
                socket.bind(endpoint)
        addresses = []
        if args.address:
            addresses.append(args.address)
        if args.connect:
            addresses.extend(args.connect)
        for endpoint in addresses:
            print('connecting to "{0}"'.format(endpoint))
            socket.connect(endpoint)

    def config_connection(self):
        context = zmq.Context()
        print("Connecting to server ...")
        print("Secure transport")

        #
        # setup Request-Response type socket
        socket = context.socket(zmq.REQ)

        # We need two certificates, one for the client and one for
        # the server. The client must know the server's public key
        # to make a CURVE connection.
        client_secret_file = os.path.join(self.secret_keys_dir, "client.key_secret")
        client_public, client_secret = zmq.auth.load_certificate(client_secret_file)
        socket.curve_secretkey = client_secret
        socket.curve_publickey = client_public

        server_public_file = os.path.join(self.public_keys_dir, "server.key")
        server_public, _ = zmq.auth.load_certificate(server_public_file)

        # The client must know the server's public key to make a CURVE connection.
        socket.curve_serverkey = server_public

        socket.connect(self.control_end_point)

        return socket

    def connect_data_pub_sub_secure_channel(self, identity_uri, pub_sub_public_key):
        print("DEBUG commbase [L122]: Connecting to Dealer ...")

        #same as in REST channel
        client_secret_file = os.path.join(self.secret_keys_dir, "client.key_secret")
        client_public, client_secret = zmq.auth.load_certificate(client_secret_file)
        self.data_ch.curve_secretkey = client_secret
        self.data_ch.curve_publickey = client_public
        print("DEBUG commbase [L131]: curve_publickey type: " + str(type(client_public)))

        # print("DEBUG commbase [L131]: pub_sub_public_key "+ str(pub_sub_public_key))
        (_, decode_pub_key) = pub_sub_public_key
        self.data_ch.curve_serverkey = decode_pub_key       # pub_sub_public_key tuple coming from Arbiter
        self.data_ch.setsockopt(zmq.IDENTITY, identity_uri)

        self.data_ch.connect(self.data_end_point)
        print("DEBUG commbase [L148]: Connected")

        return

    def get_last_ts(self, sensor_name):
        """Return the last timeseries value stored in the sensor name (or URI path).

                Args:
                   sensor_name (str):  The name of the sensor.

                Returns:
                    the value

                Simply use it to retreive the last stored value in the following way:

                >>> print myAP.get_last_ts('sensor')
        """

        uri_path = "/ts/" + sensor_name + "/last/1"
        # TODO:
        # should I change the interface to make it less explicit?
        #
        m = Zest_GET_Message_Timeseries(self.token, uri_path, ZEST_JSON_FORMAT)

        self.socket.send(m.get_message())
        rcv_message = self.socket.recv()

        return rcv_message

    def post_ts(self, sensor_name, payload):
        """Stores a timeseries value in the database.

                Args:
                   sensor_name (str):  The name of the sensor.
                   payload (str): element to be stored.


                Example to post an integer value into /foo/bar path:

                >>> myAP = Zest_Access_Point(end_point, token)
                >>> raw_msg = myAP.post_ts('/foo/bar', payload)
                >>> m = Zest_Response_Message(raw_msg)

        """
        uri_path = "/ts/" + sensor_name

        m = Zest_POST_Message_Timeseries(self.token, uri_path, ZEST_JSON_FORMAT, payload)

        print ("POST: ", m)
        self.socket.send(m.get_message())

        rcv_message = self.socket.recv()

        return rcv_message

    def get_last_n_ts(self, sensor_name, amount):
        """Return the last n timeseries values stored in the database.

        Args:
           sensor_name (str):  The name of the sensor.
           amount (int): Number of elements to retreive.

        Returns:
           str - the last n time series entries

        Simply use it to retreive 4 messages in the following way:

        >>> myAP = Zest_Access_Point(end_point, token)
        >>> raw_msg = myAP.get_last_ts(sensor_name, 4)
        >>> m = Zest_Response_Message(raw_msg)
        >>> (format, msg)  = m.get_payload()
        >>> if format == dbox_zest_message.ZEST_JSON_FORMAT:
        >>>     print(msg)

        """

        uri_path = "/ts/last/" + sensor_name + "/" + str(amount)
        m = Zest_GET_Message_Timeseries(self.token, uri_path, ZEST_JSON_FORMAT)

        self.socket.send(m)
        rcv_message = self.socket.recv()

        return rcv_message

        # returns (option_format, results)

    def decode_observe_payload(self, rcv_message):


        # TODO: decoding the message implies enhancing dbox_zest_message to decode the observe ACK
        # TODO: be careful with the structure of the program so that it elegantly solves the problem
        # TODO: of decoding the message. Remember the comment from John about the blocking call of observe
        # TODO: may have to do with the promise sematics, or not.

        msg = Zest_Response_Message(rcv_message)

        (format, pub_sub_public_key) = msg.options

        # identity value comes as payload
        identity = msg.payload

        return (identity, pub_sub_public_key)

    def connect_observer(self, uri):

        m = Zest_Observe_Data_Message(self.token, uri, ZEST_JSON_FORMAT, 0)

        #send observe request through REST control channel
        self.socket.send(m.get_message())
        #obtain credentials to connect to PUB/SUB data channel
        rcv_message = self.socket.recv()

        #TODO: get credentials from received message
        #decode message and get uuid + credentials to connect to dealer

        (identity, pub_sub_public_key) = self.decode_observe_payload(rcv_message)

        self.connect_data_pub_sub_secure_channel(identity, pub_sub_public_key)

        self.observer_id = random.randint(100000,999999)

        return self.observer_id

    # blocking call to observe messages

    def observe(self, _observer_id):
        """Sets-up a blocking observer of the database.

                        Args:
                           _observer_id(str):  ID returned during the creationg process, it is intended to validate the client code
                           (or the ownership of the observer).

                        Returns:
                           Messages from the activity of the server (e.g., POSTS).

                        In a different thread (or process) just call observe and it will block till the reception of a message:

                        >>> myAP = Zest_Access_Point(rest_end_point, pubsub_end_point, token)
                        >>> my_obs_id = myAP.connect_observer(uri_to_observe)
                        >>> while (True):
                        >>>     print(str(myAP.observe(my_obs_id)))

        """

        if (_observer_id == self.observer_id):
            msg = self.data_ch.recv()
            # print("DEBUG observe [L251]: from observer" + str(msg))
        else:
            print("ERROR [L260]: Wrong observer_id provided: " + str(_observer_id))
            exit(0)

        return msg

    def unpack_results(self, message):

        resp = Zest_Response_Message(message)

        # TODO: check the following since in client.re line 120 seems to be returning twice the same value
        # (options, payload) = handle_options(option_count, rest)

        if (resp.get_response_code() == 69):
            # TODO: missing asking if it has public key (see line 144 client.re)
            # print("OK")
            return resp.get_options()
        else:
            print("WARNING: Code " + str(int(resp.get_response_code())) + " is not handled yet!")
            return []
