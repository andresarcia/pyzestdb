"""
.. module:: dbox_zest_message
    :platform: Linux, Mac OS
    :synopsis: Message manager for ZestDB clients handling message marshaling and unmarshaling.

.. moduleauthor:: Andres Arcia-Moret<andres.arcia@cl.cam.ac.uk>

.. rubric:: dbox_zest_message.py

This module contains all functions to prepare messages for the different REST functions being executed on
the Zest server.

It contains the following classes:

*Zest_Message_Manager*
    to create a basic message out of a token, the type of message (code),
    the service path. Alternatively, depending on the type of message (GET, POST, OBSERVE, etc.) it
    will accordingly prepare the message including the relevant fields, for example, obs_type and max_age
    for an observation function.

*Zest_Message_Debugger*
    print out messages in a Zest-like format. It allows to debug the
    communication between a python client and Zest server. The hexadecimal output of the message
    (print_in_hex) can be compared to the debug output of Zest (client and server).

Other classes specialising the different protocol messages are:

*Zest_GET_Message_Timeseries*
    prepares a time series GET message.

*Zest_POST_Message_Timeseries*
    prepares a time series POST message.

Zest_Response_Message
    decode the response messages from the Zest server. It can then retrieve options, response code and payload.

Zest_Observe_Data_Message
    prepares a data observer message.

"""

ZEST_TEXT_FORMAT = 0
ZEST_BINARY_FORMAT = 42
global ZEST_JSON_FORMAT
ZEST_JSON_FORMAT = 50
ZEST_GET_MSG_CODE = 1
ZEST_POST_MSG_CODE = 2
ZEST_DELETE_MSG_CODE = 4

# May be reconsidered as a separate message later
ZEST_OBSERVE_MSG_CODE = 8

import struct
import socket
import binascii
import datetime


class Zest_Message_Manager:

    def __init__(self, token, msg_code, uri_path, payload_format, obs_type = 'data', max_age = 0):
        self.token = token
        self.uri_path = uri_path
        self.format = payload_format
        self.message_len = 0

        if (msg_code == ZEST_GET_MSG_CODE or msg_code == ZEST_POST_MSG_CODE):
            # options: generates option_count
            (options_value, options_length, options_count) = self.create_post_get_options(self.uri_path, self.format)
            self.msg_code = msg_code
        elif (msg_code == ZEST_OBSERVE_MSG_CODE):
            self.msg_code = ZEST_GET_MSG_CODE # Observe should be a message not an option or workaround
            (options_value, options_length, options_count) = self.create_observe_options(self.uri_path, self.format, obs_type, max_age)
        else:
            print("Zest_Message_Manager: message type not managed yet")
            exit(0)

        # header: receives option_count
        (header_value, header_length) = self.create_header(len(self.token), options_count, self.msg_code)
        # token
        (token_value, token_length) = self.create_token(self.token)
        # payload
        message_format = str(header_length) + "s" + str(token_length) + "s" + str(options_length) + "s"
        # assemble message
        self.message = struct.pack(message_format, header_value, token_value, options_value)
        self.message_len = header_length + token_length +  options_length

    def get_message(self):
        return self.message

    def get_message_len(self):
        return self.message_len

    def create_header(self, token_len, option_count, code):
        header_format = ">BBH"  # unsigned char (8) unsigned char (8) bigendian unsigned short (16 bits)
        header = struct.pack(header_format, code, option_count, token_len)

        return (header, 4)  # lengths are in bytes

    def create_option(self, option_id, content, forced_size_bits=0):
        # create_option must handle integers agnostic of endianess and strings

        if type(content) is str:
            option_format = ">HH" + str(len(content)) + "s"
            packed_option = struct.pack(option_format, option_id, len(content), content.encode('utf-8'))
            return (packed_option, len(content) + 4)  # lengths are in bytes
        elif type(content) is int:
            if (forced_size_bits == 32):
                option_format = ">HHL" # unsigned short, unsigned short, unsigned long
                size_of_content = 4 # in bytes (unsigned long)
            else:
                option_format = ">HHH"
                size_of_content = 2 # in bytes (unsigned)
            packed_option = struct.pack(option_format, option_id, size_of_content, content)
            return (packed_option, 2 + 2 + size_of_content)  # lengths are in bytes
        else:
            print ("create_option: I don't recognize option type")
            exit(0)

    def create_token(self, _token):
        token_format = str(len(_token)) + "s"
        token_b = struct.pack(token_format, _token.encode('utf-8'))

        return (token_b, len(_token))  # lengths are in bytes

    def create_basic_options(self, uri, content_format_type):
        (uri_path, uri_path_length) = self.create_option(11, uri)

        (uri_host, uri_host_length) = self.create_option(3, socket.gethostname())

        (content_format_code, content_format_length) = self.create_option(12, content_format_type)

        return [(uri_path, uri_path_length), (uri_host, uri_host_length), (content_format_code, content_format_length)]

    def create_post_get_options(self, uri, content_format_type):

        # total_length = uri_path_length+uri_host_length+content_format_length;

        return self.pack_options(self.create_basic_options(uri, content_format_type))


    def create_observe_options(self, uri, content_format_type, obs_type, max_age):
        # NOTE: order from John's code: uri_path, uri_host, observe, content_format, max_age

        obs_opt = self.create_basic_options(uri, content_format_type)

        obs_type_code = 6
        max_age_type_code = 14

        # append obs_type
        (obs_type_code, obs_type_format_length) = self.create_option(obs_type_code, obs_type)
        obs_opt.append((obs_type_code, obs_type_format_length))

        # append max_age
        (max_age_code, max_age_format_length) = self.create_option(max_age_type_code, max_age, 32)
        obs_opt.append((max_age_code, max_age_format_length))


        return self.pack_options(obs_opt)

    def pack_options(self, options):
        option_count = len(options)
        total_length = 0

        values_format = ''
        val_array = []

        for (value, length) in options:
            total_length = total_length + length
            values_format = values_format + str(length) + "s"
            val_array.append(value)

        packed_options = struct.pack(values_format, *val_array)

        return (packed_options, total_length, option_count)

    def add_payload(self, payload):

        return message_with_payload


class Zest_Message_Debugger:

    def __init__(self):
        return

    def print_in_hex(self, msg_byte):
        separator = 0
        col = 0
        line = 0

        now = datetime.datetime.now()
        print(str(now) + " [debug] " + str(line).zfill(8) + ": ", end='')
        msg_hex = binascii.hexlify(msg_byte)
        for l in msg_hex:
            print (chr(l), end='')
            separator += 1

            if (separator == 4):
                separator = 0
                col += 1
                print(" ", end='')

            if (col == 8):
                col = 0
                print('')
                line += 1
                now = datetime.datetime.now()
                print(str(now) + " [debug] " + str(line).zfill(8) + ": ", end='')

        print('\n')

    def print_in_dec(self, message):
        return


class Zest_GET_Message_Timeseries:

    def __init__(self, token, uri_path, msg_format):
        if msg_format == ZEST_JSON_FORMAT:
            self.msg = Zest_Message_Manager(token, ZEST_GET_MSG_CODE, uri_path, ZEST_JSON_FORMAT)
        else:
            print("GET construction Error: message type not managed")
            exit(0)

    def get_message(self):
        return self.msg.get_message()

class Zest_POST_Message_Timeseries:

    def __init__(self, token, uri_path, msg_format, payload):
        if msg_format == ZEST_JSON_FORMAT:
            self.msg =  Zest_Message_Manager(token, ZEST_POST_MSG_CODE, uri_path, ZEST_JSON_FORMAT)
            self.replace_payload(payload)

        else:
            print("POST construction Error: message type not managed")
            exit(0)

    def replace_payload(self, payload):
        message_format = str(self.msg.get_message_len()) + "s" + str(len(payload)) + "s"
        self.msg = struct.pack(message_format, self.msg.get_message(), payload.encode('utf-8'))

        return self.msg

    def get_message(self):
        return self.msg.get_message()

class Zest_Observe_Data_Message:

    def __init__(self, token, uri_path, msg_format, max_age):
        if msg_format == ZEST_JSON_FORMAT:
            self.msg =  Zest_Message_Manager(token, ZEST_OBSERVE_MSG_CODE, uri_path, ZEST_JSON_FORMAT, 'data', max_age)
        else:
            print("Observe Data building error: message type not managed")
            exit(0)

    def get_message(self):
        return self.msg.get_message()

class Zest_Response_Message:

    def __init__(self, message):
        self.payload_format = -1
        self.payload = ''
        # get first 3 elements
        (self.code, self.option_count, self.token_len, rest) = self.handle_header(message)
        # decode options
        self.options = self.handle_options(self.option_count, rest)

        # missing to see how to unpack in types and then leave the rest as a byte string
        # see lines 188 - 190 of https://github.com/me-box/zestdb/blob/master/test/client.re

    def get_options(self):
        return self.options

    def get_response_code(self):
        return self.code

    def handle_header(self, msg):
        header_format = ">BBH" + str(len(msg) - 4) + "s"

        (token_len, option_count, code, rest) = struct.unpack(header_format, msg)

        return (token_len, option_count, code, rest)

    def handle_option(self, msg):
        handle_format = ">HH" + str(len(msg) - 4) + "s"

        (number, len_opt, rest) = struct.unpack(handle_format, msg)

        handle_format = str(int(len_opt)) + "s" + str(len(rest) - int(len_opt)) + "s"

        (value, rest2) = struct.unpack(handle_format, rest)

        return (number, value, rest2)

    def handle_options(self, option_count, msg):
        if (int(option_count) == 0):
            return [(0, msg)]
        else:
            options = []
            rest = msg
            while (option_count > 0):
                (number, value, rest) = self.handle_option(rest)
                print("DEGUB [L267] option: "+str(int(number))+" value: "+str(value.decode("utf-8"))+"\n")
                options.append((number, value))
                if (number == 12):
                    (self.payload_format,) = struct.unpack(">H",value)
                option_count -= 1
            # after having processed all options the 'rest' corresponds to the payload.
            if (option_count == 0):
                self.payload = rest

            print ("DEBUG [L276] payload: "+str(self.payload))
            return options

    def get_payload(self):
        return (self.payload_format, self.payload)


# TODO: NOT CHECKED YET!
class Zest_GET_Message_KeyValue:

    def __init__(self, _token, _uri_path, _msg_format):
        self.uri_path = _uri_path
        self.msg_format = _msg_format

        if _msg_format == ZEST_JSON_FORMAT:
            self.msg = Zest_Message_Manager(_token, ZEST_GET_MSG_CODE, _uri_path, ZEST_JSON_FORMAT)
        else:
            print("Zest_GET_Message_KeyValue Error: message not managed")
            exit(0)

    def get_message(self):
        return (self.msg.get_message())

    def print_response(self, response_msg):
        (token_len, option_count, code, rest) = handle_header(response_msg)
        options = handle_options(option_count, rest)

        # TODO: check the following since in client.re line 120 seems to be returning twice the same value
        # (options, payload) = handle_options(option_count, rest)

        if (int(token_len) == 69):
            # TODO: missing asking if it has public key (see line 144 client.re)
            # print("OK")
            return options
        else:
            print("WARNING: Code " + str(int(code)) + " is not handled yet!")
            return []


class Zest_POST_Message_KeyValue:

    def __init__(self, _uri_path):
        self.uri_path = _uri_path
        self.zest_get_msg = Zest_Message_Manager()

    def prepare_json_post_req_msg(self, uri_path, payload):
        token = ""  # empty as in client.re
        msg_post_code = 2  # POST
        format = 50  # JSON

        (options_value, options_length, options_count) = create_post_get_options(uri_path, format)
        (header_value, header_length) = create_header(len(token), options_count, msg_post_code)
        (token_value, token_length) = create_token(token)

        message_format = str(header_length) + "s" + str(token_length) + "s" + str(options_length) + "s" + str(
            len(payload)) + "s"
        message = struct.pack(message_format, header_value, token_value, options_value, payload.encode('utf-8'))
        return message

    def print_response(self, response_msg):
        (code, option_count, token_len, rest) = handle_header(response_msg)
        options = handle_options(option_count, rest)
        # TODO: check the following since in client.re line 120 seems to be returning twice the same value
        # (options, payload) = handle_options(option_count, rest)

        if (int(code) == 69):
            # TODO: missing asking if it has public key (see line 144 client.re)
            # print("OK")
            return options
        elif (int(code) == 65):
            return [(65, "")]
        elif (int(code) == 128):
            print("ERROR: Bad request - check JSON format")
            exit(0)
        else:
            print("WARNING: Code " + str(int(code)) + " is not handled yet!")
            return []


